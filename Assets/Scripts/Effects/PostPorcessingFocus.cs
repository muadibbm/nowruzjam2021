﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using System.Collections;

public class PostPorcessingFocus : MonoBehaviour
{
    [SerializeField] PostProcessProfile profile;
    [SerializeField] float duration = 2.5f;
    [SerializeField] float start = 0.1f;
    [SerializeField] float target = 5f;

    private int index;

    private IEnumerator Start() {
        DepthOfField dof = profile.GetSetting<DepthOfField>();
        index = profile.settings.IndexOf(dof);
        float time = 0;
        while (time <= duration) {
            dof.focusDistance.value = Mathf.Lerp(start, target, time / duration);
            profile.settings[index] = dof;
            time += Time.deltaTime;
            yield return null;
        }
    }

    private void OnDestroy() {
        DepthOfField dof = profile.GetSetting<DepthOfField>();
        dof.focusDistance.value = start;
        profile.settings[index] = dof;
    }
}
