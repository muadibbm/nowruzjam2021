﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public void LoadScene(string name, LoadSceneMode mode) {
        SceneManager.LoadSceneAsync(name, mode);
    }

    public void UnLoadScene(string name) {
        SceneManager.UnloadSceneAsync(name, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
    }
}