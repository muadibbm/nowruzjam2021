﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using System.Runtime.InteropServices;

public class Menu : MonoBehaviour
{
    [SerializeField] HandController hc;
    [SerializeField] GameObject[] localization_English;
    [SerializeField] GameObject[] localization_Farsi;
    [SerializeField] Animator menu;
    [SerializeField] PostPorcessingFocus ppf;
    [SerializeField] PostProcessLayer layer;
    [SerializeField] PostProcessVolume volume;
    [SerializeField] AudioSource music_intro;

    private GameInput gi;

    private void Awake() {
        gi = GameManager.Instance.GetTool<GameInput>("GameInput");
        if(Application.isMobilePlatform) {
            layer.enabled = volume.enabled = false;
        }
    }

    public void SelectLanguage(string language) {
        Enum.Language selectedLanguage = (Enum.Language)System.Enum.Parse(typeof(Enum.Language), language);
        switch(selectedLanguage) {
            case Enum.Language.English:
                for (int i = 0; i < localization_English.Length; i++) {
                    localization_English[i].SetActive(true);
                }
                break;
            case Enum.Language.Farsi:
                for (int i = 0; i < localization_Farsi.Length; i++) {
                    localization_Farsi[i].SetActive(true);
                }
                break;
        }
        menu.SetTrigger("LanguageSelected");
        if(music_intro != null) music_intro.Play();
        gi.SetCursorVisibility(false);
    }

    public void StartGame() {
        ppf.enabled = true;
        hc.enabled = true;
        gi.SetCursorVisibility(true);
    }

    public void Quit() {
        //Application.Quit();
        GameManager.Instance.GetTool<SceneController>("SceneController").LoadScene("MAIN", UnityEngine.SceneManagement.LoadSceneMode.Single);
    }
}
