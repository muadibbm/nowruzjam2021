﻿using UnityEngine;
using UnityEngine.EventSystems;

public class UI_HoverButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] bool hasSFX = true;

    private HandController hc;

    private void Awake() {
        hc = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<HandController>();
    }

    public void OnPointerEnter(PointerEventData eventData) {
        hc.EnterHoverOverUI(hasSFX);
    }

    public void OnPointerExit(PointerEventData eventData) {
        hc.ExitHoverOverUI();
    }
}