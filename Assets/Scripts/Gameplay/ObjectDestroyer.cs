﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ObjectDestroyer : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        Destroy(other.transform.root.gameObject);
    }
}
