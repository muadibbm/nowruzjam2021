﻿using System;
using UnityEngine;

public class Collision : MonoBehaviour
{
    public Action onCollisionEnter;

    private void OnCollisionEnter(UnityEngine.Collision collision) {
        onCollisionEnter.Invoke();
    }
}