﻿using UnityEngine;
using UnityEngine.Events;

public class ItemIdentification : MonoBehaviour
{
    [HideInInspector] public int index;
    [HideInInspector] public UnityAction<int> onItemDestroyedCallback;
    [HideInInspector] public UnityAction<int> onItemCollisionCallback;

    private void Awake() {
        GetComponentInChildren<Collision>().onCollisionEnter = OnCollision;
    }

    private void OnCollision() {
        onItemCollisionCallback.Invoke(index);
    }

    private void OnDestroy() {
        onItemDestroyedCallback.Invoke(index);
    }
}
