﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Runtime.InteropServices;

[RequireComponent(typeof(Camera))]
public class HandController : MonoBehaviour
{
    [SerializeField] Transform cameraPivot;
    [SerializeField] Vector2 speedCameraRotation;
    [SerializeField] Vector2 cameraVerticalBounds;
    [SerializeField] Vector2 cameraVerticalRotations;
    [SerializeField] Transform spawnPoint;
    [SerializeField] Vector2 spawnScaleRange = new Vector2(1f, 1.5f);
    [SerializeField] Transform mouseWorldPoint;
    [SerializeField] LayerMask mouseSpawnLayer;
    [SerializeField] LayerMask mouseGrabLayer;
    [SerializeField] float grabLerpTime = 0.25f;
    [SerializeField] MeshFilter outline;
    [SerializeField] Item[] items;
    [SerializeField] float speedItemRotation = 1f;
    [SerializeField] Button quit_Button;
    [SerializeField] Button takePhoto_Button;
    [SerializeField] Animator takePhoto_Vfx;
    [SerializeField] Texture2D cursor_defualt = default;
    [SerializeField] Texture2D cursor_click = default;
    [SerializeField] Texture2D cursor_grab = default;
    [SerializeField] Texture2D cursor_camera_rotate = default;
    [SerializeField] Vector2 hotspot;
    [SerializeField] CursorMode cursorMode;
    [SerializeField] Animator item_name;
    [SerializeField] TMPro.TextMeshPro item_name_English;
    [SerializeField] RTLTMPro.RTLTextMeshPro3D item_name_Farsi;
    [SerializeField] Animator item_meaning;
    [SerializeField] TMPro.TextMeshProUGUI item_meaning_English;
    [SerializeField] RTLTMPro.RTLTextMeshPro item_meaning_Farsi;
    [SerializeField] float durationUntilNextItem = 5f;
    [SerializeField] AudioSource[] sfx_takePhoto;
    [SerializeField] AudioSource[] sfx_ui;
    [SerializeField] AudioSource completion_music;
    [SerializeField] Animator baseItemsTutorial;
    [SerializeField] Animator cameraTutorial;
    [SerializeField] CanvasGroup frameCanvas;


    [Serializable] 
    public struct Item
    {
        public GameObject prefab;
        public Button button;
        public Color color;
        public string name_English;
        public string name_Farsi;
        public string meaning_English;
        public string meaning_Farsi;
        public AudioSource[] sfx_spawn;
        public AudioSource[] sfx_pickup;
        public AudioSource[] sfx_drop;
        public AudioSource[] sfx_destroy;
        public AudioSource[] sfx_collision;
        public bool alreadySpawned;
    }

    //private AudioSource lastPlayedSpawn;
    private Camera cam;
    private RaycastHit hitDataSpawn;
    private RaycastHit hitDataGrab;
    private GameInput gi;
    private Transform grabbedItem;
    private Vector2 prevMousePosition;
    private Coroutine grabRoutine;
    private bool cursorOverUI = false;
    private int currentIndex;
    private bool elementUIHasSFX = false;
    private bool baseItemsTutorialActivated = false;
    private bool baseItemsTutorialCompleted = false;
    private MeshFilter meshFilter;

    private void Awake() {
        gi = GameManager.Instance.GetTool<GameInput>("GameInput");
        cam = GetComponent<Camera>();
    }

    private void OnEnable() {
        ColorBlock cb = items[0].button.colors;
        cb.normalColor = items[0].color;
        items[0].button.colors = cb;
        items[0].button.image.color = items[0].color;
    }

    private void Update() {
        UpdateMouseWorldPoint();
        if(Input.GetKey("`") && Input.GetKey("1") && Input.GetKeyDown("2")) {
            TakePhoto();
        }
        if (grabbedItem) {
            Cursor.SetCursor(cursor_grab, hotspot, cursorMode);
            grabbedItem.Rotate(Vector3.up, gi.MouseScrollDelta.y * speedItemRotation);
            if (gi.MouseButtonLeft == false) {
                DropItem();
            }
        } else if (gi.MouseButtonLeftDown) {
            TryToGrabItem();
        } else { 
            if(gi.MouseButtonRight) {
                cameraPivot.Rotate(0f, (gi.MousePosition.x - prevMousePosition.x) * speedCameraRotation.x * Time.deltaTime, 0f);
                Cursor.SetCursor(cursor_camera_rotate, hotspot, cursorMode);

                Vector3 pos = cam.transform.localPosition;
                pos.y = Mathf.Clamp(pos.y - (gi.MousePosition.y - prevMousePosition.y) * speedCameraRotation.y * Time.deltaTime, cameraVerticalBounds.x, cameraVerticalBounds.y);
                cam.transform.localPosition = pos;
                Vector3 euler = cam.transform.localRotation.eulerAngles;
                euler.x = GetRelativeFloatValue(cameraVerticalRotations.x, cameraVerticalRotations.y, cameraVerticalBounds.x, cameraVerticalBounds.y, pos.y);
                cam.transform.localRotation = Quaternion.Euler(euler);

                outline.gameObject.SetActive(false);
                outline.transform.parent = null;
            }
            else {
                if (cursorOverUI) {
                    Cursor.SetCursor(cursor_click, hotspot, cursorMode);
                } else {
                    if (Physics.Raycast(cam.ScreenPointToRay(gi.MousePosition), out hitDataGrab, 100, mouseGrabLayer, QueryTriggerInteraction.Ignore)) {
                        Cursor.SetCursor(cursor_click, hotspot, cursorMode);
                        meshFilter = hitDataGrab.transform.GetComponentsInChildren<MeshFilter>()[0];
                        outline.mesh = meshFilter.mesh;
                        outline.transform.parent = meshFilter.transform;
                        outline.transform.localPosition = Vector3.zero;
                        outline.transform.localRotation = Quaternion.identity;
                        outline.transform.localScale = Vector3.one;
                        outline.gameObject.SetActive(true);
                    } else {
                        Cursor.SetCursor(cursor_defualt, hotspot, cursorMode);
                        outline.gameObject.SetActive(false);
                        outline.transform.parent = null;
                    }
                }
            }
        }
        if(cursorOverUI && gi.MouseButtonLeftDown && elementUIHasSFX) {
            if(sfx_ui != null && sfx_ui.Length != 0) sfx_ui[UnityEngine.Random.Range(0, sfx_ui.Length)].Play();
        }
        prevMousePosition = gi.MousePosition;
    }

    public void EnterHoverOverUI(bool hasSFX) {
        cursorOverUI = true;
        elementUIHasSFX = hasSFX;
    }

    public void ExitHoverOverUI() {
        cursorOverUI = false;
    }

    private void UpdateMouseWorldPoint() {
        if (Physics.Raycast(cam.ScreenPointToRay(gi.MousePosition), out hitDataSpawn, 1000, mouseSpawnLayer, QueryTriggerInteraction.Ignore)) {
            mouseWorldPoint.position = hitDataSpawn.point;
        }
    }

    private bool TryToGrabItem() {
        if (Physics.Raycast(cam.ScreenPointToRay(gi.MousePosition), out hitDataGrab, 1000, mouseGrabLayer, QueryTriggerInteraction.Ignore)) {
            grabbedItem = hitDataGrab.transform;
            grabbedItem.parent = mouseWorldPoint;
            grabbedItem.GetComponent<Rigidbody>().isKinematic = true;
            grabbedItem.localRotation = Quaternion.FromToRotation(grabbedItem.up, Vector3.up) * grabbedItem.localRotation;
            currentIndex = grabbedItem.GetComponent<ItemIdentification>().index;
            if (items[currentIndex].sfx_pickup != null && items[currentIndex].sfx_pickup.Length != 0)
                items[currentIndex].sfx_pickup[UnityEngine.Random.Range(0, items[currentIndex].sfx_pickup.Length)].Play();
            else
                Debug.LogWarning("Missing SFX_PICKUP of " + items[currentIndex].name_English);
            if (grabRoutine != null) StopCoroutine(grabRoutine);
            grabRoutine = StartCoroutine(Grab());
            // Outline object
            meshFilter = grabbedItem.GetComponentsInChildren<MeshFilter>()[0];
            outline.mesh = meshFilter.mesh;
            outline.transform.parent = meshFilter.transform;
            outline.transform.localPosition = Vector3.zero;
            outline.transform.localRotation = Quaternion.identity;
            outline.transform.localScale = Vector3.one;
            outline.gameObject.SetActive(true);
            return true;
        }
        return false;
    }

    private IEnumerator Grab() {
        Vector3 startPos = grabbedItem.localPosition;
        float time = 0;
        while(time <= grabLerpTime) {
            grabbedItem.localPosition = Vector3.Lerp(startPos, Vector3.zero, time / grabLerpTime);
            time += Time.deltaTime;
            yield return null;
        }
        grabRoutine = null;
    }

    private void DropItem() {
        if (items[currentIndex].sfx_drop != null && items[currentIndex].sfx_drop.Length != 0)
            items[currentIndex].sfx_drop[UnityEngine.Random.Range(0, items[currentIndex].sfx_drop.Length)].Play();
        else
            Debug.LogWarning("Missing SFX_DROP of " + items[currentIndex].name_English);
        if (grabRoutine != null) StopCoroutine(grabRoutine);
        grabRoutine = null;
        grabbedItem.GetComponent<Rigidbody>().isKinematic = false;
        grabbedItem.parent = null;
        grabbedItem = null;
        outline.transform.parent = null;
        outline.gameObject.SetActive(false);
    }

    public void SpawnItem(int index) {
        if (items[index].alreadySpawned == false) {
            items[index].button.GetComponent<Animator>().SetTrigger("Idle");
            items[index].alreadySpawned = true;
        }
        GameObject spawnedItem = Instantiate(items[index].prefab, spawnPoint.position, Quaternion.identity);
        spawnedItem.AddComponent<Collision>();
        ItemIdentification item = spawnedItem.AddComponent<ItemIdentification>();
        item.index = index;
        item.onItemDestroyedCallback = OnItemDestroyedCallback;
        item.onItemCollisionCallback = OnItemCollisionCallback;
        if (items[index].sfx_spawn != null && items[index].sfx_spawn.Length != 0)
            items[index].sfx_spawn[UnityEngine.Random.Range(0, items[index].sfx_spawn.Length)].Play();
        else
            Debug.LogWarning("Missing SFX_SPAWN of " + items[index].name_English);
        spawnedItem.transform.localScale = UnityEngine.Random.Range(spawnScaleRange.x, spawnScaleRange.y) * spawnedItem.transform.localScale;
        if (index == items.Length - 1) {
            if (revealItemRoutine != null) return;
            revealItemRoutine = StartCoroutine(RevealNameAndMeaning(index)); // do this only once
        } else {
            if (items[index + 1].button.gameObject.activeSelf || revealItemRoutine != null) return;
            revealItemRoutine = StartCoroutine(RevealNameAndMeaning(index)); // do this only once and then activate the next item spawn button
        }
    }

    private void OnItemCollisionCallback(int index) {
        if (items[index].sfx_collision != null && items[index].sfx_collision.Length != 0) {
            int rand = UnityEngine.Random.Range(0, items[index].sfx_collision.Length);
            if(items[index].sfx_collision[rand].isPlaying == false)
                items[index].sfx_collision[rand].Play();
        } else {
            Debug.LogWarning("Missing SFX_COLLISION of " + items[index].name_English);
        }
    }

    private void OnItemDestroyedCallback(int index) {
        if (items[index].sfx_destroy != null && items[index].sfx_destroy.Length != 0)
            items[index].sfx_destroy[UnityEngine.Random.Range(0, items[index].sfx_destroy.Length)].Play();
        else
            Debug.LogWarning("Missing SFX_DESTROY of " + items[index].name_English);
    }

    private Coroutine revealItemRoutine;

    private IEnumerator RevealNameAndMeaning(int index) {
        item_name_English.text = items[index].name_English;
        item_name_Farsi.text = items[index].name_Farsi;
        item_name_English.color = item_name_Farsi.color = items[index].color;
        item_meaning_English.text = items[index].meaning_English;
        item_meaning_Farsi.text = items[index].meaning_Farsi;
        item_name.SetTrigger("Show");
        item_meaning.SetTrigger("Show");
        yield return new WaitForSeconds(durationUntilNextItem);
        item_name.SetTrigger("Hide");
        item_meaning.SetTrigger("Hide");
        if (index == items.Length - 1) {
            StartCoroutine(ShowCameraTutorial());
        } else {
            if (index == 6 && baseItemsTutorialActivated == false) {
                baseItemsTutorialActivated = true;
                StartCoroutine(ShowBaseItemsTutorial(index));
            }
            if (baseItemsTutorialActivated && baseItemsTutorialCompleted == false) {
                yield break;
            }
            UnlockNextObject(index);
        }
    }

    private void UnlockNextObject(int index) {
        ColorBlock cb = items[index + 1].button.colors;
        cb.normalColor = items[index + 1].color;
        items[index + 1].button.colors = cb;
        items[index + 1].button.image.color = items[index + 1].color;
        items[index + 1].button.gameObject.SetActive(true);
        revealItemRoutine = null;
    }

    private IEnumerator ShowBaseItemsTutorial(int index) {
        baseItemsTutorial.SetTrigger("Show");
        yield return new WaitForSeconds(durationUntilNextItem);
        baseItemsTutorial.SetTrigger("Hide");
        baseItemsTutorialCompleted = true;
        UnlockNextObject(index);
    }

    private IEnumerator ShowCameraTutorial() {
        completion_music.Play();
        cameraTutorial.SetTrigger("Show");
        yield return new WaitForSeconds(durationUntilNextItem);
        cameraTutorial.SetTrigger("Hide");
        takePhoto_Button.gameObject.SetActive(true);
    }

    public void TakePhoto() {
        sfx_takePhoto[UnityEngine.Random.Range(0, sfx_takePhoto.Length)].Play();
        StartCoroutine(TakePhotoRoutine());
    }

    private IEnumerator TakePhotoRoutine() {
        for (int i = 0; i < items.Length; i++) {
            items[i].button.gameObject.SetActive(false);
        }
        takePhoto_Button.gameObject.SetActive(false);
        quit_Button.gameObject.SetActive(false);
        gi.SetCursorVisibility(false);
        takePhoto_Vfx.SetTrigger("TakePhoto");
        yield return new WaitForSeconds(1f);
        frameCanvas.alpha = 1f;
        if (Application.platform == RuntimePlatform.WebGLPlayer) {
            StartCoroutine(UploadPNG());
        } else {
            int index = 0;
            string path = Application.persistentDataPath + "/HaftSin_Photo_";
            while (System.IO.File.Exists(path + index + ".jpg")) {
                index++;
                yield return null;
            }
            yield return new WaitForEndOfFrame();
            ScreenCapture.CaptureScreenshot(path + index + ".jpg", 2);
            Application.OpenURL("file://" + Application.persistentDataPath);
        }
        yield return new WaitForEndOfFrame();
        frameCanvas.alpha = 0f;
        for (int i = 0; i < items.Length; i++) {
            items[i].button.gameObject.SetActive(true);
            items[i].alreadySpawned = false;
        }
        takePhoto_Button.gameObject.SetActive(true);
        quit_Button.gameObject.SetActive(true);
        gi.SetCursorVisibility(true);
        yield break;
    }

    private IEnumerator UploadPNG() {
        // We should only read the screen after all rendering is complete
        yield return new WaitForEndOfFrame();

        // Create a texture the size of the screen, RGB24 format
        int width = Screen.width;
        int height = Screen.height;
        var tex = new Texture2D(width, height, TextureFormat.RGB24, false);

        // Read screen contents into the texture
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();

        // Encode texture into PNG
        byte[] bytes = tex.EncodeToPNG();
        Destroy(tex);

        //string ToBase64String byte[]
        string encodedText = System.Convert.ToBase64String(bytes);

        //var image_url = "data:image/png;base64," + encodedText;

        SaveScreenshotWebGL("HaftSin_Photo", encodedText);
    }

    [DllImport("__Internal")]
    private static extern void SaveScreenshotWebGL(string filename, string data);

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(spawnPoint.position, 0.1f);
    }

    protected float GetRelativeFloatValue(float a, float b, float ta, float tb, float tx) {
        return a + (b - a) * (tx - ta) / (tb - ta);
    }
}
