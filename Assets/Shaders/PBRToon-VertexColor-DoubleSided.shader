Shader "IllTaco/PBR Toon Vertex Colors (Double Sided)"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo", 2D) = "white" {}

	_Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5

		_Glossiness("Smoothness", Range(0.0, 1.0)) = 0.5
		_GlossMapScale("Smoothness Scale", Range(0.0, 1.0)) = 1.0
		[Enum(Metallic Alpha,0,Albedo Alpha,1)] _SmoothnessTextureChannel("Smoothness texture channel", Float) = 0

		[Gamma] _Metallic("Metallic", Range(0.0, 1.0)) = 0.0
		_MetallicGlossMap("Metallic", 2D) = "white" {}

	[ToggleOff] _SpecularHighlights("Specular Highlights", Float) = 1.0
		[ToggleOff] _GlossyReflections("Glossy Reflections", Float) = 1.0

		_BumpScale("Scale", Float) = 1.0
		_BumpMap("Normal Map", 2D) = "bump" {}

	//_Parallax("Height Scale", Range(0.005, 0.08)) = 0.02
		//_ParallaxMap("Height Map", 2D) = "black" {}

	_OcclusionStrength("Strength", Range(0.0, 1.0)) = 1.0
		_OcclusionMap("Occlusion", 2D) = "white" {}

	//_EmissionColor("Color", Color) = (0,0,0)
		//_EmissionMap("Emission", 2D) = "white" {}

	//_DetailMask("Detail Mask", 2D) = "white" {}
	//_DetailAlbedoMap("Detail Albedo x2", 2D) = "grey" {}
	//_DetailNormalMapScale("Scale", Float) = 1.0
	//	_DetailNormalMap("Normal Map", 2D) = "bump" {}

	[Enum(UV0,0,UV1,1)] _UVSec("UV Set for secondary textures", Float) = 0


		// Blending state
		[HideInInspector] _Mode("__mode", Float) = 0.0
		[HideInInspector] _SrcBlend("__src", Float) = 1.0
		[HideInInspector] _DstBlend("__dst", Float) = 0.0
		[HideInInspector] _ZWrite("__zw", Float) = 1.0

		// Vertex Color
		_IntensityVC("Vertex Color Intencity", Float) = 1.0

		// Toon Ramp
		_ColorBase("Base Color", Color) = (0.5, 0.5, 0.5, 1.0)
		_ColorShade("Shade Color", Color) = (0.3, 0.3, 0.3, 1.0)
		_ShadeAmount("Shade Amount", Range(0, 1)) = 0.4
		_ColorHighlight("Highlight Color", Color) = (0.8, 0.8, 0.8, 1.0)
		_HighlightAmount("Highlight Amount", Range(0, 1)) = 0.2
		_PBRBlendAmount("PBR Blend Amount", Range(0, 1)) = 0.5
	}

	CGINCLUDE
#define UNITY_SETUP_BRDF_INPUT MetallicSetup
		ENDCG

		SubShader
	{
		Tags{ "RenderType" = "Opaque" "PerformanceChecks" = "False" }
		LOD 300


		// ------------------------------------------------------------------
		//  Base forward pass (directional light, emission, lightmaps, ...)
		Pass
	{
		Name "FORWARD"
		Tags{ "LightMode" = "ForwardBase" }

		Blend[_SrcBlend][_DstBlend]
		ZWrite[_ZWrite]
		Cull Off


		CGPROGRAM
#pragma target 3.0

		// -------------------------------------

#pragma shader_feature _NORMALMAP
#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
#pragma shader_feature _EMISSION
#pragma shader_feature _METALLICGLOSSMAP 
#pragma shader_feature ___ _DETAIL_MULX2
#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
#pragma shader_feature _ _SPECULARHIGHLIGHTS_OFF
#pragma shader_feature _ _GLOSSYREFLECTIONS_OFF
#pragma shader_feature _PARALLAXMAP
#pragma shader_feature _VERTEXCOLOR_OFF _VERTEXCOLOR _VERTEXCOLOR_LERP

#pragma multi_compile_fwdbase
#pragma multi_compile_fog

#pragma vertex vertForwardBase_VC
//#pragma fragment fragForwardBase_VC
#pragma fragment fragForwardBase_VC_toon

#include "UnityStandardCore.cginc"
#include "UnityVC.cginc"

		// Toon 
		float4 _ColorBase;
		float4 _ColorShade;
		float4 _ColorHighlight;
		float _ShadeAmount;
		float _HighlightAmount;
		float _PBRBlendAmount;

		half4 GetToonRampAlbedo(VertexOutputForwardBase_VC i, FragmentCommonData f)
		{
			/*
			float3 lightDir = _WorldSpaceLightPos0.xyz;
			float atten = dot(f.normalWorld, lightDir) * -0.5 + 0.5; // range [0,1]
			half4 texColor = 1;
			//texColor.rgb = i.tex;
			//texColor.rgb = f.diffColor;
			texColor = i.color;
			// Lerp between shade color and main color
			//half4 shadeCol = lerp(i.tex, _ColorShade, 1-_PBRBlendAmount);
			half4 shadeCol = _ColorShade;
			return shadeCol;
			//half4 toonCol = lerp(texColor, shadeCol, step(1 - _ShadeAmount, atten));
			half4 toonCol = i.tex * _ColorShade * step(1 - _ShadeAmount, atten);
			// Lerp between highlight color and shade/main color
			half4 highlightCol = lerp(i.tex, _ColorHighlight, 1-_PBRBlendAmount);
			toonCol = lerp(toonCol, highlightCol, step(atten, _HighlightAmount));
			UNITY_APPLY_FOG(i.fogCoord, toonCol);
			return toonCol;
			*/

			/**/
			float3 lightDir = _WorldSpaceLightPos0.xyz;
			float atten = dot(f.normalWorld, lightDir) * -0.5 + 0.5; // range [0,1]
			half4 texColor = half4(1, 1, 1, 1);
			texColor.rgb = f.diffColor;
#if _VERTEXCOLOR
			texColor.rgb = i.color.rgb;
#endif
			// Lerp btwn shade color and main color
			half4 rampColor = lerp(_ColorBase, _ColorShade, step(1 - _ShadeAmount, atten));
			// Lerp btwen highlight color and shade/main color
			rampColor = lerp(rampColor, _ColorHighlight, step(atten, _HighlightAmount));
			half4 c = rampColor * texColor;
			UNITY_APPLY_FOG(i.fogCoord, c);
			return c;
			/**/

		}
		half4 fragForwardBase_VC_toon(VertexOutputForwardBase_VC i) : SV_Target
		{
			FRAGMENT_SETUP(s)
	#if UNITY_OPTIMIZE_TEXCUBELOD
				s.reflUVW = i.reflUVW;
	#endif

			half4 diffColor = GetToonRampAlbedo(i, s); //toon

			UnityLight mainLight = MainLight();// (s.normalWorld);
			half atten = SHADOW_ATTENUATION(i);

			half occlusion = Occlusion(i.tex.xy);
			UnityGI gi = FragmentGI(s, occlusion, i.ambientOrLightmapUV, atten, mainLight);

			half4 c = UNITY_BRDF_PBS(s.diffColor, s.specColor, s.oneMinusReflectivity, s.smoothness, s.normalWorld, -s.eyeVec, gi.light, gi.indirect);
	#if _VERTEXCOLOR
			c *= i.color * _IntensityVC;
	#endif
	#if _VERTEXCOLOR_LERP
			c *= lerp(half4(1,1,1,1), i.color, _IntensityVC);
	#endif
			c.rgb += UNITY_BRDF_GI(s.diffColor, s.specColor, s.oneMinusReflectivity, s.smoothness, s.normalWorld, -s.eyeVec, occlusion, gi);
			c.rgb += Emission(i.tex.xy); //i.color.a * s.diffColor;

			UNITY_APPLY_FOG(i.fogCoord, c.rgb);
	#if _VERTEXCOLOR
			s.alpha *= i.color.a;
	#endif
	#if _VERTEXCOLOR_LERP
			s.alpha *= lerp(1, i.color.a, _IntensityVC);
	#endif

			c = lerp(diffColor, c, _PBRBlendAmount);
			return OutputForward(c, s.alpha);
		}


		ENDCG
	}
		// ------------------------------------------------------------------
		//  Additive forward pass (one light per pass)
		Pass
	{
		Name "FORWARD_DELTA"
		Tags{ "LightMode" = "ForwardAdd" }
		Blend[_SrcBlend] One
		Fog{ Color(0,0,0,0) } // in additive pass fog should be black
		ZWrite Off
		ZTest LEqual
		Cull Off

		CGPROGRAM
#pragma target 3.0

		// -------------------------------------

#pragma shader_feature _NORMALMAP
#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
#pragma shader_feature _METALLICGLOSSMAP
#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
#pragma shader_feature _ _SPECULARHIGHLIGHTS_OFF
#pragma shader_feature ___ _DETAIL_MULX2
#pragma shader_feature _PARALLAXMAP
#pragma shader_feature _VERTEXCOLOR

#pragma multi_compile_fwdadd_fullshadows
#pragma multi_compile_fog

#pragma vertex vertForwardAdd_VC
#pragma fragment fragForwardAdd_VC

#include "UnityStandardCore.cginc"
#include "UnityVC.cginc"
		ENDCG
	}
		// ------------------------------------------------------------------
		//  Shadow rendering pass
		Pass
	{
		Name "ShadowCaster"
		Tags{ "LightMode" = "ShadowCaster" }

		ZWrite On ZTest LEqual
		Cull Off

		CGPROGRAM
#pragma target 3.0

		// -------------------------------------

#pragma shader_feature _VERTEXCOLOR
#pragma shader_feature _VERTEXCOLOR_LERP
#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
#pragma multi_compile_shadowcaster

#pragma vertex vertShadowCaster_VC
#pragma fragment fragShadowCaster_VC

#include "UnityStandardShadow.cginc"
#include "UnityVCShadow.cginc"

		ENDCG
	}
		// ------------------------------------------------------------------
		//  Deferred pass
		Pass
	{
		Name "DEFERRED"
		Tags{ "LightMode" = "Deferred" }
		Cull Off
		CGPROGRAM
#pragma target 3.0
#pragma exclude_renderers nomrt

		// -------------------------------------

#pragma shader_feature _NORMALMAP
#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
#pragma shader_feature _EMISSION
#pragma shader_feature _METALLICGLOSSMAP
#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
#pragma shader_feature _ _SPECULARHIGHLIGHTS_OFF
#pragma shader_feature ___ _DETAIL_MULX2
#pragma shader_feature _PARALLAXMAP
#pragma shader_feature _VERTEXCOLOR_OFF _VERTEXCOLOR _VERTEXCOLOR_LERP

#pragma multi_compile ___ UNITY_HDR_ON
#pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
#pragma multi_compile ___ DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
#pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON

#pragma vertex vertDeferred_VC
#pragma fragment fragDeferred_VC

#include "UnityStandardCore.cginc"
#include "UnityVC.cginc"

		ENDCG
	}

		// ------------------------------------------------------------------
		// Extracts information for lightmapping, GI (emission, albedo, ...)
		// This pass it not used during regular rendering.
		Pass
	{
		Name "META"
		Tags{ "LightMode" = "Meta" }

		Cull Off

		CGPROGRAM
#pragma vertex vert_meta
#pragma fragment frag_meta

#pragma shader_feature _EMISSION
#pragma shader_feature _METALLICGLOSSMAP
#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
#pragma shader_feature ___ _DETAIL_MULX2

#include "UnityStandardMeta.cginc"
		ENDCG
	}
	}

		SubShader
	{
		Tags{ "RenderType" = "Opaque" "PerformanceChecks" = "False" }
		LOD 150

		// ------------------------------------------------------------------
		//  Base forward pass (directional light, emission, lightmaps, ...)
		Pass
	{
		Name "FORWARD"
		Tags{ "LightMode" = "ForwardBase" }

		Blend[_SrcBlend][_DstBlend]
		ZWrite[_ZWrite]
		Cull Off
		CGPROGRAM
#pragma target 2.0

#pragma shader_feature _NORMALMAP
#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
#pragma shader_feature _EMISSION 
#pragma shader_feature _METALLICGLOSSMAP 
#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
#pragma shader_feature _ _SPECULARHIGHLIGHTS_OFF
#pragma shader_feature _ _GLOSSYREFLECTIONS_OFF
#pragma shader_feature _VERTEXCOLOR_OFF _VERTEXCOLOR _VERTEXCOLOR_LERP

		// SM2.0: NOT SUPPORTED shader_feature ___ _DETAIL_MULX2
		// SM2.0: NOT SUPPORTED shader_feature _PARALLAXMAP

#pragma skip_variants SHADOWS_SOFT DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE

#pragma multi_compile_fwdbase
#pragma multi_compile_fog

#pragma vertex vertForwardBase_VC
#pragma fragment fragForwardBase_VC

#include "UnityStandardCore.cginc"
#include "UnityVC.cginc"

		ENDCG
	}
		// ------------------------------------------------------------------
		//  Additive forward pass (one light per pass)
		Pass
	{
		Name "FORWARD_DELTA"
		Tags{ "LightMode" = "ForwardAdd" }
		Blend[_SrcBlend] One
		Fog{ Color(0,0,0,0) } // in additive pass fog should be black
		ZWrite Off
		ZTest LEqual
		Cull Off
		CGPROGRAM
#pragma target 2.0

#pragma shader_feature _NORMALMAP
#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
#pragma shader_feature _METALLICGLOSSMAP
#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
#pragma shader_feature _ _SPECULARHIGHLIGHTS_OFF
#pragma shader_feature ___ _DETAIL_MULX2
		// SM2.0: NOT SUPPORTED shader_feature _PARALLAXMAP
#pragma skip_variants SHADOWS_SOFT
#pragma shader_feature _VERTEXCOLOR

#pragma multi_compile_fwdadd_fullshadows
#pragma multi_compile_fog

#pragma vertex vertForwardAdd_VC
#pragma fragment fragForwardAdd_VC

#include "UnityStandardCore.cginc"
#include "UnityVC.cginc"
		ENDCG
	}
		// ------------------------------------------------------------------
		//  Shadow rendering pass
		Pass
	{
		Name "ShadowCaster"
		Tags{ "LightMode" = "ShadowCaster" }

		ZWrite On ZTest LEqual
		Cull Off
		CGPROGRAM
#pragma target 2.0

#pragma shader_feature _VERTEXCOLOR
#pragma shader_feature _VERTEXCOLOR_LERP
#pragma shader_feature _ _ALPHATEST_ON _ALPHABLEND_ON _ALPHAPREMULTIPLY_ON
#pragma skip_variants SHADOWS_SOFT
#pragma multi_compile_shadowcaster

#pragma vertex vertShadowCaster_VC
#pragma fragment fragShadowCaster_VC

#include "UnityStandardShadow.cginc"
#include "UnityVCShadow.cginc"

		ENDCG
	}

		// ------------------------------------------------------------------
		// Extracts information for lightmapping, GI (emission, albedo, ...)
		// This pass it not used during regular rendering.
		Pass
	{
		Name "META"
		Tags{ "LightMode" = "Meta" }

		Cull Off

		CGPROGRAM
#pragma vertex vert_meta
#pragma fragment frag_meta

#pragma shader_feature _EMISSION
#pragma shader_feature _METALLICGLOSSMAP
#pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
#pragma shader_feature ___ _DETAIL_MULX2

#include "UnityStandardMeta.cginc"
		ENDCG
	}
	}

		FallBack "VertexLit"
		//CustomEditor "StandardShaderVCGUI"
}
