using UnityEngine;

public class PottedPlantMotor : MonoBehaviour
{
	[SerializeField] Transform plantAnchorBase;
	[SerializeField] Transform plantTransform;
	[SerializeField] float heightOffset = 5f;
	[SerializeField] float trackSpeed = 8f;
	[SerializeField] float plantTiltClamp = 1f;

	Transform plantTarget;
	Vector3 plantTargetDesiredPos;

	private void Awake()
	{
		var plantTargetOb = new GameObject("PlantTarget");
		plantTargetOb.hideFlags = HideFlags.HideInHierarchy;
		plantTarget = plantTargetOb.transform;
	}

	private void Update()
	{
		plantTargetDesiredPos = plantAnchorBase.position + (plantAnchorBase.up * heightOffset);
		
		plantTarget.position = Vector3.Lerp(plantTarget.position, plantTargetDesiredPos, Time.deltaTime * trackSpeed);
		
		//Clamp plant tilt
		Vector3 rel = plantTarget.position - plantTargetDesiredPos;

		if (rel.magnitude > plantTiltClamp)
		{
			rel = Vector3.ClampMagnitude(rel, plantTiltClamp);
			plantTarget.position = plantTargetDesiredPos + rel;
		}

		plantTransform.rotation = Quaternion.LookRotation(plantTarget.position - plantTransform.position, plantAnchorBase.right) * Quaternion.Euler(90f, 0f, 0f);
	}

	private void OnDrawGizmos()
	{
		if (Application.isPlaying)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(plantTargetDesiredPos, 0.5f);

			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(plantTarget.position, 0.5f);
		}
	}
}
